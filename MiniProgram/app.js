//app.js
App({
  onLaunch: function () {
  },
  globalData: {
    userInfo: null,  //用户信息

    baseWechatUrl:"http://localhost:5208/api/WeChatService",  //访问路径
    webSocketUrl:"ws://localhost:5208",  //WebSocket 地址

    openId:'', //用户唯一标识
    loginState:false  , //用户登录状态
  }
})